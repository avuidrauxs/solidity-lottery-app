require('dotenv').config()

const HDWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const { interface, bytecode } = require('./compile');

const provider = new HDWalletProvider(process.env.WALLET_MNEMONIC, process.env.RINKEBY_NETWORK);

const web3 = new Web3(provider);

const deploy = async () => {
  const accounts = await web3.eth.getAccounts();
  
  console.log('Attempting to deploy using address ', accounts[0]);
  
  const result = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({ data: bytecode })
    .send({ gas: 6500000, gasPrice: '100000000000' ,from: accounts[0]})
    .on('error', (error) => { console.log(error) });
    
  console.log(`Contract deployed to ${result.options.address} was successful`);
};

deploy();
