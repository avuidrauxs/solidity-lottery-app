const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const { interface, bytecode } = require('../compile');

const web3 = new Web3(ganache.provider());

let lottery;
let accounts;

beforeEach(async () => {
  accounts = await web3.eth.getAccounts();
  
  lottery = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({ data: bytecode })
    .send({ gas: 6500000, gasPrice: '100000000000', from: accounts[0]})
    .on('error', (error) => { console.log(error) });
})

describe('Lottery', () => {
  it('actually deploys a contract', () => {
    assert.ok(lottery.options.address);
  });
  
  it("should allow us to enter one player", async () => {
    await lottery.methods.enter()
      .send({ from: accounts[0], value: web3.utils.toWei('0.02', 'ether') });
  
    const players = await lottery.methods.getPlayers()
      .call({ from: accounts[0] });
    
    assert.equal(accounts[0], players[0]);
    assert.equal(1, players.length);
  });
  
  
  it('should allow us to enter more players', async () => {
    await lottery.methods.enter()
      .send({ from: accounts[0], value: web3.utils.toWei('0.02', 'ether') });

    await lottery.methods.enter()
      .send({ from: accounts[3], value: web3.utils.toWei('0.02', 'ether') });
  
    const players = await lottery.methods.getPlayers()
      .call({ from: accounts[0] });
    
    assert.equal(accounts[0], players[0]);
    assert.equal(accounts[3], players[1]);
    assert.equal(2, players.length);
  });
  
  
  it('requires a minimum amount of ether to enter', async () => {
    try {
      await lottery.methods.enter()
        .send({ from: accounts[0], value: 0 });
        assert(false);
    } catch(error) {
      assert(error);
    }
  });
  
  it('should only allow manager to pick winner', async () => {
    try {
      await lottery.methods.pickWinner()
        .send({ from: accounts[1] });
        assert(false);
    } catch(error) {
      assert(error);
    }
  });
  
  it('sends money to winner and resets the players array', async () => {
    await lottery.methods.enter()
      .send({ from: accounts[0], value: web3.utils.toWei('2', 'ether') });
      
    const initialBalance = await web3.eth.getBalance(accounts[0]);
    await lottery.methods.pickWinner().send({ from: accounts[0] });
    const finalBalance = await web3.eth.getBalance(accounts[0]);
    const diffrence = finalBalance - initialBalance;
    const players = await lottery.methods.getPlayers()
      .call({ from: accounts[0] });
      
    assert(diffrence > web3.utils.toWei('1.8', 'ether'));
    assert(players.length === 0);
  });
  
  
});